﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Enumerations
{
    public enum PizzaTopping
    {
        Margherita = 0,
        QuattroFormaggi = 200,
        QuattroStagioni = 150,
        Pepperoni = 100,
        BbqChicken = 100

    }
}
