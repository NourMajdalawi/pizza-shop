﻿using System;

namespace Enumerations
{
    public enum DrinkType
    {
        FlatWater = 250,
        SparklingWater = 250,
        Cola = 275,
        IceTea = 275,
        EnergyDrink = 375

    }
}
