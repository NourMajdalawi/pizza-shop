﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Pizza : Product
    {
        private PizzaCrust pizzaCrust;
        private PizzaTopping pizzaTopping;
        public const double basePrice = 599; 

        public Pizza(PizzaCrust pizzaCrust, PizzaTopping pizzaTopping)
        {
            this.Id = id++;
            this.pizzaCrust = pizzaCrust;
            this.pizzaTopping = pizzaTopping;
            this.name = "Pizza " + pizzaTopping.ToString() + "  " + pizzaCrust.ToString();
            this.price = basePrice + Convert.ToDouble(pizzaTopping) + Convert.ToDouble(pizzaCrust);
        }
        
    }
}
