﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Order
    {
        public int Id { get; private set; }
        public static int id { get; set; }

        public int customerId { get; private set; }
        public DateTime orderedAt { get; private set; }
        public bool completed { get; private set; }
        public string productsAsString { get; private set; }
        public double price { get; private set; }
        public List<Product> products { get; private set; }

        public Order(int customerId, DateTime orderedAt, bool completed, List<Product> products)
        {
            this.Id = id++;
            this.customerId = customerId;
            this.orderedAt = orderedAt;
            this.completed = completed;
            this.productsAsString = ParseProductsToString(products);
            this.price = GetTotalPrice(products);
        }

        public Order(int orderId, int customerId, DateTime orderedAt, bool completed, string productsAsString, double price)
        {
            this.Id = orderId;
            id++;
            this.customerId = customerId;
            this.orderedAt = orderedAt;
            this.completed = completed;
            this.productsAsString = productsAsString;
            this.price = price;
        }

        public void CompleteOrder()
        {
            completed = true;
        }


        public double GetTotalPrice(List<Product> products)
        {
            double price = 0;
            foreach(Product product in products)
            {
                price += product.GetPrice();
            }
            return price;
        }

        public string ParseProductsToString(List<Product> products)
        {
            string productsAsString = "";
            foreach (Product product in products)
            {
                if(products.IndexOf(product) < products.Count)
                {
                    productsAsString += product.name;
                }
                if(products.IndexOf(product) < products.Count - 1)
                {
                    productsAsString += ",";
                }
            }
            return productsAsString;
        }
    }
}
