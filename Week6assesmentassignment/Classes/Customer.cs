﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Customer
    {
        private static int id = 0;
        public int Id { get;}
        public string firstName { get; private set; }
        public string lastName { get; private set; }
        public string emailAddress { get; private set; }

        public List<Order> orders{ get; private set; }

        public Customer(string firstName, string lastName, string emailAddress)
        {
            this.Id = id++;
            this.firstName = firstName;
            this.lastName = lastName;
            this.emailAddress = emailAddress;
            orders = new List<Order>();
        }

        public Customer(int customerId, string firstName, string lastName, string emailAddress, List<Order> orders)
        {
            this.Id = customerId;
            id += 1 ;
            this.firstName = firstName;
            this.lastName = lastName;
            this.emailAddress = emailAddress;
            if (orders.Count > 0)
            {
                this.orders = orders;
            } else
            {
                this.orders = new List<Order>();
            }
        }

        public void AddOrder(Order order)
        {
            this.orders.Add(order);
        }

        public void CancelOrder(int orderId)
        {
            foreach (Order order in orders)
            {
                if (order.Id == orderId)
                {
                    orders.Remove(order);
                    return;
                }
            }
        }

        public void CompleteOrder(int orderId)
        {
            foreach(Order order in orders)
            {
                if(order.Id == orderId)
                {
                    order.CompleteOrder();
                }
            }
        }

        public void EditInfo(string firstName, string lastName, string emailAddress)
        {
            if(firstName != "")
            {
                this.firstName = firstName;
            }
            if (lastName != "")
            {
                this.lastName = lastName;
            }
            if (emailAddress != "")
            {
                this.emailAddress = emailAddress;
            }
        }

        public Order GetOrderById(int orderId)
        {
            foreach (Order order in orders)
            {
                if (order.Id == orderId)
                {
                    return order;
                }
            }
            return null;
        }

        public string GetInfo()
        {
            string info = "";
            info += "Name: " + firstName + " " + lastName + ". Email address: " + emailAddress;
            return info;
        }
    }
}
