﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public abstract class Product 
    {
        public static int id = 0;
        public int Id { get; set; }
        public int orderId { get; private set; }
        public string name { get; set; }
        public double price { get; set; }

        public double GetPrice()
        {
            //Convert price to euro's and return it
            return price / 100;
        }
       
    }
}
