﻿using Enumerations;
using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Drink : Product
    {
        private DrinkType drinkType;

        public Drink(DrinkType drinkType)
        {
            this.Id = id++;
            this.drinkType = drinkType;
            this.name = drinkType.ToString();
            this.price = Convert.ToDouble(drinkType);
        }
        
    }
}
