﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Classes
{
    public class DataHelper
    {

        public void WriteData(List<Customer> customers)
        {
            List<Order> orders = new List<Order>();
            AddAllCustomers(customers);
            foreach(Customer customer in customers)
            {
                foreach(Order order in customer.orders)
                {
                    orders.Add(order);
                }
            }
            if (orders != null)
            {
                AddAllOrders(orders);
            }
        }
        public void AddAllCustomers(List<Customer> customers)
        {
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                fs = new FileStream("CustomerData", FileMode.OpenOrCreate, FileAccess.Write);
                sw = new StreamWriter(fs);

                foreach(Customer customer in customers)
                {
                    sw.WriteLine("CUSTOMER-" + customer.Id + "-" + customer.firstName + "-" + customer.lastName + "-" + customer.emailAddress);
                }
            }
            catch(IOException)
            {

            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }


        public void AddAllOrders(List<Order> orders)
        {
            FileStream fs = null;
            StreamWriter sw = null;

            try
            {
                fs = new FileStream("OrderData", FileMode.OpenOrCreate, FileAccess.Write);
                sw = new StreamWriter(fs);

                foreach(Order order in orders)
                {
                    sw.WriteLine("ORDER-" + order.Id + "-" + order.customerId + "-" + order.orderedAt + "-" + order.completed + "-" + order.productsAsString + "-" + order.price);
                }
            }
            catch (IOException)
            {

            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
            }
        }

        public void EditCustomer()
        {

        }

        public List<Customer> LoadData()
        {
            List<Customer> customers = new List<Customer>();
            List<Order> allOrders = LoadOrders();
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                fs = new FileStream("CustomerData", FileMode.OpenOrCreate, FileAccess.Read);
                sr = new StreamReader(fs);

                string customerString = sr.ReadLine();
                while(customerString != null)
                {
                    List<string> words = customerString.Split('-').ToList();
                    if(words[0] != "CUSTOMER")
                    {
                        break;
                    }
                    int customerId = Convert.ToInt32(words[1]);
                    string firstName = words[2];
                    string lastName = words[3];
                    string emailAddress = words[4];
                    Customer customer = new Customer(customerId, firstName, lastName, emailAddress, new List<Order>());
                    foreach (Order order in allOrders)
                    {
                        if(order.customerId == customerId)
                        {
                            customer.orders.Add(order);
                        }
                    }
                    customers.Add(customer);
                    customerString = sr.ReadLine();

                }

            }
            catch(IOException ex)
            {
                throw ex;
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                }
            }
            return customers;
        }

        public List<Order> LoadOrders()
        {
            List<Order> loadedOrders = new List<Order>();
            FileStream fs = null;
            StreamReader sr = null;

            try
            {
                fs = new FileStream("OrderData", FileMode.Open, FileAccess.Read);
                sr = new StreamReader(fs);

                string orderString = sr.ReadLine();
                while (orderString != null)
                {
                    List<string> words = orderString.Split('-').ToList();
                    if (words[0] != "ORDER")
                    {
                        break;
                    }
                    int orderId = Convert.ToInt32(words[1]);
                    int customerId = Convert.ToInt32(words[2]);
                    DateTime orderedAt = Convert.ToDateTime(words[3]);
                    bool completed = Convert.ToBoolean(words[4]);
                    string productsAsString = words[5];
                    double totalPrice = Convert.ToDouble(words[6]);

                    loadedOrders.Add(new Order(orderId, customerId, orderedAt, completed, productsAsString, totalPrice));
                    orderString = sr.ReadLine();
                }
                return loadedOrders;
            }
            catch
            {
                return null;
            }
            finally
            {
                if(sr != null)
                {
                    sr.Close();
                }
            }
        }

    }
}
