﻿using System;
using System.Collections.Generic;

namespace Classes
{
    public class Shop
    {
        string name;
        public List<Customer> customers { get; private set; }

        public Shop(string name)
        {
            this.name = name;
            customers = new List<Customer>();
        }

        public void AddCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public void EditCustomer(int currentCustomerId, string firstName, string lastName, string emailAddress)
        {
            for(int i = 0; i < customers.Count; i++)
            {
                if(customers[i].Id == currentCustomerId)
                {
                    customers[i].EditInfo(firstName, lastName, emailAddress);
                }
            }
        }

        public Customer GetCustomerById(int customerId)
        {
            foreach(Customer customer in customers)
            {
                if(customer.Id == customerId)
                {
                    return customer;
                }
            }
            return null;
        }
    }
}
