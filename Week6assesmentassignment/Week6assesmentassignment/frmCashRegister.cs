﻿using Classes;
using Enumerations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week6assesmentassignment
{
    public partial class frmOverview : Form
    {
        Shop shop;
        List<Product> products;
        DataHelper dh;
        public frmOverview()
        {
            InitializeComponent();
            shop = new Shop("Pizzashop");
            dh = new DataHelper();
            LoadData();
            products = new List<Product>();
        }

        private void btnAddCustomer_Click(object sender, EventArgs e)
        {
            string firstName = tbFirstName.Text;
            string lastName = tbLastName.Text;
            string emailAddress = tbEmailAddress.Text;
            if (firstName == "" || lastName == "" || emailAddress == "")
            {
                MessageBox.Show("Please fill in all fields before adding a customer.");
                return;
            }
            Customer customer = new Customer(firstName, lastName, emailAddress);
            shop.AddCustomer(customer);
            dh.WriteData(shop.customers);
            RefreshCustomers();
            tbFirstName.Clear();
            tbLastName.Clear();
            tbEmailAddress.Clear();
        }

        private void btnEditCustomer_Click(object sender, EventArgs e)
        {
            string firstName = tbFirstName.Text;
            string lastName = tbLastName.Text;
            string emailAddress = tbEmailAddress.Text;
            if (firstName == "" && lastName == "" && emailAddress == "")
            {
                MessageBox.Show("Please fill in at least one field before editing a customer.");
                return;
            }
            int customerId = GetSelectedCustomerIdFromCustomers();
            if (customerId < 0)
            {
                MessageBox.Show("Please select a customer before attempting to edit a customer.");
                return;
            }
            shop.EditCustomer(customerId, firstName, lastName, emailAddress);
            RefreshCustomers();
            tbFirstName.Clear();
            tbLastName.Clear();
            tbEmailAddress.Clear();
        }

        private void btnCancelOrder_Click(object sender, EventArgs e)
        {
            int orderId = GetSelectedOrderIdFromOrders();
            if (orderId < 0)
            {
                MessageBox.Show("Please select an order before attempting to confirm an order.");
                return;
            }
            int customerId = GetSelectedCustomerIdFromOrders();
            if (customerId < 0)
            {
                MessageBox.Show("No customer ID found for that order.");
                return;
            }
            if (shop.GetCustomerById(customerId).GetOrderById(orderId).completed == true)
            {
                MessageBox.Show("You can't cancel orders that have already been completed.");
                return;
            }
            shop.GetCustomerById(customerId).CancelOrder(orderId);
            RefreshOrders();
        }

        private void btnCompleteOrder_Click(object sender, EventArgs e)
        {
            int orderId = GetSelectedOrderIdFromOrders();
            if (orderId < 0)
            {
                MessageBox.Show("Please select an order before attempting to confirm an order.");
                return;
            }
            int customerId = GetSelectedCustomerIdFromOrders();
            if (customerId < 0)
            {
                MessageBox.Show("No customer ID found for that order.");
                return;
            }
            Customer customer = shop.GetCustomerById(customerId);
            customer.GetOrderById(orderId).CompleteOrder();
            RefreshOrders();
        }

        public int GetSelectedCustomerIdFromCustomers()
        {
            if (dgvCustomers.SelectedCells != null)
            {
                return -1;
            }
            int SelectedCustomerRowIndex = dgvCustomers.SelectedCells[0].RowIndex;
            DataGridViewRow selectedCustomerRow = dgvCustomers.Rows[SelectedCustomerRowIndex];
            int customerId = Convert.ToInt32(selectedCustomerRow.Cells["customerId"].Value);
            return customerId;
        }

        public int GetSelectedCustomerIdFromOrders()
        {
            if (dgvOrders.SelectedCells.Count < 1)
            {
                return -1;
            }
            int SelectedOrderRowIndex = dgvOrders.SelectedCells[0].RowIndex;
            DataGridViewRow selectedOrderRow = dgvOrders.Rows[SelectedOrderRowIndex];
            int customerId = Convert.ToInt32(selectedOrderRow.Cells["orderCustomerId"].Value);
            return customerId;
        }

        public int GetSelectedOrderIdFromOrders()
        {
            if (dgvOrders.SelectedCells.Count < 1)
            {
                return -1;
            }
            int SelectedOrderRowIndex = dgvOrders.SelectedCells[0].RowIndex;
            DataGridViewRow selectedOrderRow = dgvOrders.Rows[SelectedOrderRowIndex];
            int orderId = Convert.ToInt32(selectedOrderRow.Cells["orderId"].Value);
            return orderId;
        }

        public int GetSelectedProductIdFromProducts()
        {
            if (dgvProducts.SelectedCells == null)
            {
                return -1;
            }
            int SelectedProductRowIndex = dgvProducts.SelectedCells[0].RowIndex;
            DataGridViewRow selectedProductRow = dgvProducts.Rows[SelectedProductRowIndex];
            int productId = Convert.ToInt32(selectedProductRow.Cells["productId"].Value);
            return productId;
        }

        private void btnAddPizza_Click(object sender, EventArgs e)
        {
            if (cbbCustomer.SelectedItem == null)
            {
                MessageBox.Show("Please select a customer before trying to place an order.");
                return;
            }
            PizzaTopping pizzaTopping;
            PizzaCrust pizzaCrust;

            if (rbMargherita.Checked)
            {
                pizzaTopping = PizzaTopping.Margherita;
            }
            else if (rbQuattroFormaggi.Checked)
            {
                pizzaTopping = PizzaTopping.QuattroFormaggi;
            }
            else if (rbQuattroStagioni.Checked)
            {
                pizzaTopping = PizzaTopping.QuattroStagioni;
            }
            else if (rbPepperoni.Checked)
            {
                pizzaTopping = PizzaTopping.Pepperoni;
            }
            else if (rbBbqChicken.Checked)
            {
                pizzaTopping = PizzaTopping.BbqChicken;
            }
            else
            {
                MessageBox.Show("Select a pizza topping before adding a pizza to your order.");
                return;
            }

            if (rbThin.Checked)
            {
                pizzaCrust = PizzaCrust.Thin;
            }
            else if (rbThick.Checked)
            {
                pizzaCrust = PizzaCrust.Thick;
            }
            else if (rbFilled.Checked)
            {
                pizzaCrust = PizzaCrust.Filled;
            }
            else
            {
                MessageBox.Show("Select a pizza crust before adding a pizza to your order.");
                return;
            }

            Pizza pizza = new Pizza(pizzaCrust, pizzaTopping);
            products.Add(pizza);
            RefreshProducts();
        }

        private void btnAddDrink_Click(object sender, EventArgs e)
        {
            if (cbbCustomer.SelectedItem == null)
            {
                MessageBox.Show("Please select a customer before trying to place an order.");
                return;
            }
            DrinkType drinkType;

            if (rbFlatWater.Checked)
            {
                drinkType = DrinkType.FlatWater;
            }
            else if (rbSparklingWater.Checked)
            {
                drinkType = DrinkType.SparklingWater;
            }
            else if (rbCola.Checked)
            {
                drinkType = DrinkType.Cola;
            }
            else if (rbIceTea.Checked)
            {
                drinkType = DrinkType.IceTea;
            }
            else if (rbEnergyDrink.Checked)
            {
                drinkType = DrinkType.EnergyDrink;
            }
            else
            {
                MessageBox.Show("Please select a drink before adding it to your order.");
                return;
            }
            Drink drink = new Drink(drinkType);
            products.Add(drink);
            RefreshProducts();
        }

        private void btnRemoveProduct_Click(object sender, EventArgs e)
        {
            int productId = GetSelectedProductIdFromProducts();
            if (productId < 0)
            {
                MessageBox.Show("Please select a product before attempting to remove a product.");
                return;
            }

            foreach (Product product in products)
            {
                if (product.Id == productId)
                {
                    products.RemoveAt(products.IndexOf(product));
                    RefreshProducts();
                    return;
                }
            }
        }
        private void btnPlaceOrder_Click(object sender, EventArgs e)
        {
            tbTotalPrice.Clear();
            int customerIndex = cbbCustomer.SelectedIndex;
            if (customerIndex < 0)
            {
                MessageBox.Show("Please select a customer before attempting to place an order");
                return;
            }
            if (products.Count < 1)
            {
                MessageBox.Show("Please add at least one product before attempting to place an order.");
                return;
            }
            Order order = new Order(shop.customers[customerIndex].Id, DateTime.Now, false, products);

            shop.customers[customerIndex].AddOrder(order);
            dh.WriteData(shop.customers);
            RefreshOrders();
            dgvProducts.Rows.Clear();
            products.Clear();
        }

        public void RefreshCustomers()
        {
            dgvCustomers.Rows.Clear();
            cbbCustomer.Items.Clear();
            foreach (Customer customer in shop.customers)
            {
                cbbCustomer.Items.Add(customer.firstName + " " + customer.lastName);
                dgvCustomers.Rows.Add(customer.Id, customer.firstName, customer.lastName, customer.emailAddress);
            }
        }

        public void RefreshOrders()
        {
            dgvOrders.Rows.Clear();

            foreach (Customer customer in shop.customers)
            {
                for (int i = 0; i < customer.orders.Count(); i++)
                {
                    if (cbShowCompletedOrders.Checked == false && customer.orders[i].completed == false)
                    {
                        dgvOrders.Rows.Add(customer.orders[i].Id, customer.Id, customer.GetInfo(), customer.orders[i].productsAsString, customer.orders[i].price);
                    }
                    else if (cbShowCompletedOrders.Checked == true && customer.orders[i].completed == true)
                    {
                        dgvOrders.Rows.Add(customer.orders[i].Id, customer.Id, customer.GetInfo(), customer.orders[i].productsAsString, customer.orders[i].price);
                    }
                }
            }
        }

        public void RefreshProducts()
        {
            dgvProducts.Rows.Clear();
            double totalPrice = 0;
            foreach (Product product in products)
            {
                dgvProducts.Rows.Add(product.Id, product.name, product.GetPrice());
                totalPrice += product.GetPrice();
            }
            tbTotalPrice.Text = "$" + totalPrice.ToString();
        }

        private void cbShowCompletedOrders_CheckedChanged(object sender, EventArgs e)
        {
            RefreshOrders();
        }

        private void btnShowAllOrders_Click(object sender, EventArgs e)
        {
            frmAllOrdersAndRevenue allOrdersAndRevenueForm = new frmAllOrdersAndRevenue(shop, dh);
            allOrdersAndRevenueForm.Show();
        }

        private void btnSortByPrice_Click(object sender, EventArgs e)
        {
            dgvOrders.Sort(dgvOrders.Columns[4], ListSortDirection.Descending);
        }

        public void LoadData()
        {
            List<Customer> customers = dh.LoadData();
            foreach (Customer customer in customers)
            {
                shop.AddCustomer(customer);
            }
            RefreshCustomers();
            RefreshOrders();
        }

        private void tbTotalPrice_TextChanged(object sender, EventArgs e)
        {

        }

        private void cbbCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
