﻿using Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Week6assesmentassignment
{
    public partial class frmAllOrdersAndRevenue : Form
    {
        Shop shop;
        DataHelper dh;
        public frmAllOrdersAndRevenue(Shop shop, DataHelper dh)
        {
            InitializeComponent();
            this.shop = shop;
            RefreshOrders();
            this.dh = dh;
        }

        private void cbFilterByDate_CheckedChanged(object sender, EventArgs e)
        {
            RefreshOrders();
        }

        public void RefreshOrders()
        {
            double totalPrice = 0;
            dgvAllOrders.Rows.Clear();
            if (cbFilterByDate.Checked)
            {
                foreach (Customer customer in shop.customers)
                {
                    foreach (Order order in customer.orders)
                    {
                        if (order.orderedAt.Date == dtpFilterDate.Value.Date)
                        {
                            dgvAllOrders.Rows.Add(order.Id, customer.Id, customer.GetInfo(), order.productsAsString, order.price);
                            totalPrice += order.price;
                        }
                    }
                }
            }
            else
            {
                foreach (Customer customer in shop.customers)
                {
                    foreach (Order order in customer.orders)
                    {
                        dgvAllOrders.Rows.Add(order.Id, customer.Id, customer.GetInfo(), order.productsAsString, order.price);
                        totalPrice += order.price;
                    }
                }
            }
            tbTotalRevenue.Text = "$" + totalPrice.ToString();
        }

        private void dtpFilterDate_ValueChanged(object sender, EventArgs e)
        {
            RefreshOrders();
        }

        private void btnSortOrdersByPrice_Click(object sender, EventArgs e)
        {
            dgvAllOrders.Sort(dgvAllOrders.Columns[4], ListSortDirection.Descending);
        }

        private void tbTotalRevenue_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
