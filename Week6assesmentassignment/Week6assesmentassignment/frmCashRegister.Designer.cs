﻿namespace Week6assesmentassignment
{
    partial class frmOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tctrlCashRegister = new System.Windows.Forms.TabControl();
            this.tpgCustomers = new System.Windows.Forms.TabPage();
            this.gbCustomer = new System.Windows.Forms.GroupBox();
            this.dgvCustomers = new System.Windows.Forms.DataGridView();
            this.customerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerFirstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerLastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.customerEmailAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnEditCustomer = new System.Windows.Forms.Button();
            this.btnAddCustomer = new System.Windows.Forms.Button();
            this.lblCustomerEmailAddress = new System.Windows.Forms.Label();
            this.lblCustomerLastName = new System.Windows.Forms.Label();
            this.lblCustomerFirstName = new System.Windows.Forms.Label();
            this.tbFirstName = new System.Windows.Forms.TextBox();
            this.tbEmailAddress = new System.Windows.Forms.TextBox();
            this.tbLastName = new System.Windows.Forms.TextBox();
            this.tpgOverview = new System.Windows.Forms.TabPage();
            this.gbOrders = new System.Windows.Forms.GroupBox();
            this.btnShowAllOrders = new System.Windows.Forms.Button();
            this.btnCancelOrder = new System.Windows.Forms.Button();
            this.cbShowCompletedOrders = new System.Windows.Forms.CheckBox();
            this.btnSortByPrice = new System.Windows.Forms.Button();
            this.btnCompleteOrder = new System.Windows.Forms.Button();
            this.dgvOrders = new System.Windows.Forms.DataGridView();
            this.orderId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderCustomerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderCustomerInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderProducts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tpgPlaceOrder = new System.Windows.Forms.TabPage();
            this.tbTotalPrice = new System.Windows.Forms.TextBox();
            this.lblTotalPrice = new System.Windows.Forms.Label();
            this.dgvProducts = new System.Windows.Forms.DataGridView();
            this.productId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gbPizzaCrust = new System.Windows.Forms.GroupBox();
            this.rbFilled = new System.Windows.Forms.RadioButton();
            this.rbThick = new System.Windows.Forms.RadioButton();
            this.rbThin = new System.Windows.Forms.RadioButton();
            this.lblCustomer = new System.Windows.Forms.Label();
            this.cbbCustomer = new System.Windows.Forms.ComboBox();
            this.btnAddPizza = new System.Windows.Forms.Button();
            this.btnRemoveProduct = new System.Windows.Forms.Button();
            this.btnPlaceOrder = new System.Windows.Forms.Button();
            this.gbDrinks = new System.Windows.Forms.GroupBox();
            this.rbEnergyDrink = new System.Windows.Forms.RadioButton();
            this.rbIceTea = new System.Windows.Forms.RadioButton();
            this.rbCola = new System.Windows.Forms.RadioButton();
            this.rbSparklingWater = new System.Windows.Forms.RadioButton();
            this.rbFlatWater = new System.Windows.Forms.RadioButton();
            this.btnAddDrink = new System.Windows.Forms.Button();
            this.gbPizzaTopping = new System.Windows.Forms.GroupBox();
            this.rbBbqChicken = new System.Windows.Forms.RadioButton();
            this.rbPepperoni = new System.Windows.Forms.RadioButton();
            this.rbQuattroStagioni = new System.Windows.Forms.RadioButton();
            this.rbQuattroFormaggi = new System.Windows.Forms.RadioButton();
            this.rbMargherita = new System.Windows.Forms.RadioButton();
            this.tctrlCashRegister.SuspendLayout();
            this.tpgCustomers.SuspendLayout();
            this.gbCustomer.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).BeginInit();
            this.tpgOverview.SuspendLayout();
            this.gbOrders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).BeginInit();
            this.tpgPlaceOrder.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).BeginInit();
            this.gbPizzaCrust.SuspendLayout();
            this.gbDrinks.SuspendLayout();
            this.gbPizzaTopping.SuspendLayout();
            this.SuspendLayout();
            // 
            // tctrlCashRegister
            // 
            this.tctrlCashRegister.Controls.Add(this.tpgCustomers);
            this.tctrlCashRegister.Controls.Add(this.tpgOverview);
            this.tctrlCashRegister.Controls.Add(this.tpgPlaceOrder);
            this.tctrlCashRegister.Location = new System.Drawing.Point(17, 16);
            this.tctrlCashRegister.Margin = new System.Windows.Forms.Padding(4);
            this.tctrlCashRegister.Name = "tctrlCashRegister";
            this.tctrlCashRegister.SelectedIndex = 0;
            this.tctrlCashRegister.Size = new System.Drawing.Size(1499, 780);
            this.tctrlCashRegister.TabIndex = 0;
            // 
            // tpgCustomers
            // 
            this.tpgCustomers.Controls.Add(this.gbCustomer);
            this.tpgCustomers.Location = new System.Drawing.Point(4, 25);
            this.tpgCustomers.Margin = new System.Windows.Forms.Padding(4);
            this.tpgCustomers.Name = "tpgCustomers";
            this.tpgCustomers.Size = new System.Drawing.Size(1491, 751);
            this.tpgCustomers.TabIndex = 2;
            this.tpgCustomers.Text = "Customers";
            this.tpgCustomers.UseVisualStyleBackColor = true;
            // 
            // gbCustomer
            // 
            this.gbCustomer.Controls.Add(this.dgvCustomers);
            this.gbCustomer.Controls.Add(this.btnEditCustomer);
            this.gbCustomer.Controls.Add(this.btnAddCustomer);
            this.gbCustomer.Controls.Add(this.lblCustomerEmailAddress);
            this.gbCustomer.Controls.Add(this.lblCustomerLastName);
            this.gbCustomer.Controls.Add(this.lblCustomerFirstName);
            this.gbCustomer.Controls.Add(this.tbFirstName);
            this.gbCustomer.Controls.Add(this.tbEmailAddress);
            this.gbCustomer.Controls.Add(this.tbLastName);
            this.gbCustomer.Location = new System.Drawing.Point(4, 4);
            this.gbCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.gbCustomer.Name = "gbCustomer";
            this.gbCustomer.Padding = new System.Windows.Forms.Padding(4);
            this.gbCustomer.Size = new System.Drawing.Size(1463, 724);
            this.gbCustomer.TabIndex = 6;
            this.gbCustomer.TabStop = false;
            this.gbCustomer.Text = "Customer";
            // 
            // dgvCustomers
            // 
            this.dgvCustomers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCustomers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.customerId,
            this.customerFirstName,
            this.customerLastName,
            this.customerEmailAddress});
            this.dgvCustomers.Location = new System.Drawing.Point(259, 21);
            this.dgvCustomers.Margin = new System.Windows.Forms.Padding(4);
            this.dgvCustomers.Name = "dgvCustomers";
            this.dgvCustomers.ReadOnly = true;
            this.dgvCustomers.RowHeadersWidth = 51;
            this.dgvCustomers.Size = new System.Drawing.Size(1188, 678);
            this.dgvCustomers.TabIndex = 8;
            // 
            // customerId
            // 
            this.customerId.HeaderText = "Id:";
            this.customerId.MinimumWidth = 6;
            this.customerId.Name = "customerId";
            this.customerId.ReadOnly = true;
            this.customerId.Width = 125;
            // 
            // customerFirstName
            // 
            this.customerFirstName.HeaderText = "First name:";
            this.customerFirstName.MinimumWidth = 6;
            this.customerFirstName.Name = "customerFirstName";
            this.customerFirstName.ReadOnly = true;
            this.customerFirstName.Width = 150;
            // 
            // customerLastName
            // 
            this.customerLastName.HeaderText = "Last name:";
            this.customerLastName.MinimumWidth = 6;
            this.customerLastName.Name = "customerLastName";
            this.customerLastName.ReadOnly = true;
            this.customerLastName.Width = 200;
            // 
            // customerEmailAddress
            // 
            this.customerEmailAddress.HeaderText = "Email address:";
            this.customerEmailAddress.MinimumWidth = 400;
            this.customerEmailAddress.Name = "customerEmailAddress";
            this.customerEmailAddress.ReadOnly = true;
            this.customerEmailAddress.Width = 400;
            // 
            // btnEditCustomer
            // 
            this.btnEditCustomer.Location = new System.Drawing.Point(12, 201);
            this.btnEditCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.btnEditCustomer.Name = "btnEditCustomer";
            this.btnEditCustomer.Size = new System.Drawing.Size(239, 63);
            this.btnEditCustomer.TabIndex = 7;
            this.btnEditCustomer.Text = "Edit selected customer";
            this.btnEditCustomer.UseVisualStyleBackColor = true;
            this.btnEditCustomer.Click += new System.EventHandler(this.btnEditCustomer_Click);
            // 
            // btnAddCustomer
            // 
            this.btnAddCustomer.Location = new System.Drawing.Point(12, 130);
            this.btnAddCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddCustomer.Name = "btnAddCustomer";
            this.btnAddCustomer.Size = new System.Drawing.Size(239, 63);
            this.btnAddCustomer.TabIndex = 6;
            this.btnAddCustomer.Text = "Add customer";
            this.btnAddCustomer.UseVisualStyleBackColor = true;
            this.btnAddCustomer.Click += new System.EventHandler(this.btnAddCustomer_Click);
            // 
            // lblCustomerEmailAddress
            // 
            this.lblCustomerEmailAddress.AutoSize = true;
            this.lblCustomerEmailAddress.Location = new System.Drawing.Point(9, 90);
            this.lblCustomerEmailAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerEmailAddress.Name = "lblCustomerEmailAddress";
            this.lblCustomerEmailAddress.Size = new System.Drawing.Size(101, 17);
            this.lblCustomerEmailAddress.TabIndex = 5;
            this.lblCustomerEmailAddress.Text = "Email address:";
            // 
            // lblCustomerLastName
            // 
            this.lblCustomerLastName.AutoSize = true;
            this.lblCustomerLastName.Location = new System.Drawing.Point(8, 57);
            this.lblCustomerLastName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerLastName.Name = "lblCustomerLastName";
            this.lblCustomerLastName.Size = new System.Drawing.Size(78, 17);
            this.lblCustomerLastName.TabIndex = 4;
            this.lblCustomerLastName.Text = "Last name:";
            // 
            // lblCustomerFirstName
            // 
            this.lblCustomerFirstName.AutoSize = true;
            this.lblCustomerFirstName.Location = new System.Drawing.Point(9, 25);
            this.lblCustomerFirstName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomerFirstName.Name = "lblCustomerFirstName";
            this.lblCustomerFirstName.Size = new System.Drawing.Size(78, 17);
            this.lblCustomerFirstName.TabIndex = 3;
            this.lblCustomerFirstName.Text = "First name:";
            // 
            // tbFirstName
            // 
            this.tbFirstName.Location = new System.Drawing.Point(117, 21);
            this.tbFirstName.Margin = new System.Windows.Forms.Padding(4);
            this.tbFirstName.Name = "tbFirstName";
            this.tbFirstName.Size = new System.Drawing.Size(132, 22);
            this.tbFirstName.TabIndex = 0;
            // 
            // tbEmailAddress
            // 
            this.tbEmailAddress.Location = new System.Drawing.Point(117, 86);
            this.tbEmailAddress.Margin = new System.Windows.Forms.Padding(4);
            this.tbEmailAddress.Name = "tbEmailAddress";
            this.tbEmailAddress.Size = new System.Drawing.Size(132, 22);
            this.tbEmailAddress.TabIndex = 2;
            // 
            // tbLastName
            // 
            this.tbLastName.Location = new System.Drawing.Point(117, 53);
            this.tbLastName.Margin = new System.Windows.Forms.Padding(4);
            this.tbLastName.Name = "tbLastName";
            this.tbLastName.Size = new System.Drawing.Size(132, 22);
            this.tbLastName.TabIndex = 1;
            // 
            // tpgOverview
            // 
            this.tpgOverview.Controls.Add(this.gbOrders);
            this.tpgOverview.Location = new System.Drawing.Point(4, 25);
            this.tpgOverview.Margin = new System.Windows.Forms.Padding(4);
            this.tpgOverview.Name = "tpgOverview";
            this.tpgOverview.Padding = new System.Windows.Forms.Padding(4);
            this.tpgOverview.Size = new System.Drawing.Size(1491, 751);
            this.tpgOverview.TabIndex = 0;
            this.tpgOverview.Text = "Order overview";
            this.tpgOverview.UseVisualStyleBackColor = true;
            // 
            // gbOrders
            // 
            this.gbOrders.Controls.Add(this.btnShowAllOrders);
            this.gbOrders.Controls.Add(this.btnCancelOrder);
            this.gbOrders.Controls.Add(this.cbShowCompletedOrders);
            this.gbOrders.Controls.Add(this.btnSortByPrice);
            this.gbOrders.Controls.Add(this.btnCompleteOrder);
            this.gbOrders.Controls.Add(this.dgvOrders);
            this.gbOrders.Location = new System.Drawing.Point(4, 4);
            this.gbOrders.Margin = new System.Windows.Forms.Padding(4);
            this.gbOrders.Name = "gbOrders";
            this.gbOrders.Padding = new System.Windows.Forms.Padding(4);
            this.gbOrders.Size = new System.Drawing.Size(1461, 721);
            this.gbOrders.TabIndex = 6;
            this.gbOrders.TabStop = false;
            this.gbOrders.Text = "Orders";
            // 
            // btnShowAllOrders
            // 
            this.btnShowAllOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.btnShowAllOrders.Location = new System.Drawing.Point(895, 23);
            this.btnShowAllOrders.Margin = new System.Windows.Forms.Padding(4);
            this.btnShowAllOrders.Name = "btnShowAllOrders";
            this.btnShowAllOrders.Size = new System.Drawing.Size(300, 86);
            this.btnShowAllOrders.TabIndex = 7;
            this.btnShowAllOrders.Text = "View order history and revenue";
            this.btnShowAllOrders.UseVisualStyleBackColor = true;
            this.btnShowAllOrders.Click += new System.EventHandler(this.btnShowAllOrders_Click);
            // 
            // btnCancelOrder
            // 
            this.btnCancelOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.btnCancelOrder.Location = new System.Drawing.Point(9, 26);
            this.btnCancelOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancelOrder.Name = "btnCancelOrder";
            this.btnCancelOrder.Size = new System.Drawing.Size(191, 86);
            this.btnCancelOrder.TabIndex = 6;
            this.btnCancelOrder.Text = "Cancel selected order";
            this.btnCancelOrder.UseVisualStyleBackColor = true;
            this.btnCancelOrder.Click += new System.EventHandler(this.btnCancelOrder_Click);
            // 
            // cbShowCompletedOrders
            // 
            this.cbShowCompletedOrders.AutoSize = true;
            this.cbShowCompletedOrders.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.cbShowCompletedOrders.Location = new System.Drawing.Point(419, 46);
            this.cbShowCompletedOrders.Margin = new System.Windows.Forms.Padding(4);
            this.cbShowCompletedOrders.Name = "cbShowCompletedOrders";
            this.cbShowCompletedOrders.Size = new System.Drawing.Size(359, 39);
            this.cbShowCompletedOrders.TabIndex = 5;
            this.cbShowCompletedOrders.Text = "Show completed orders";
            this.cbShowCompletedOrders.UseVisualStyleBackColor = true;
            this.cbShowCompletedOrders.CheckedChanged += new System.EventHandler(this.cbShowCompletedOrders_CheckedChanged);
            // 
            // btnSortByPrice
            // 
            this.btnSortByPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.btnSortByPrice.Location = new System.Drawing.Point(1203, 23);
            this.btnSortByPrice.Margin = new System.Windows.Forms.Padding(4);
            this.btnSortByPrice.Name = "btnSortByPrice";
            this.btnSortByPrice.Size = new System.Drawing.Size(251, 86);
            this.btnSortByPrice.TabIndex = 4;
            this.btnSortByPrice.Text = "Sort by price";
            this.btnSortByPrice.UseVisualStyleBackColor = true;
            this.btnSortByPrice.Click += new System.EventHandler(this.btnSortByPrice_Click);
            // 
            // btnCompleteOrder
            // 
            this.btnCompleteOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.25F);
            this.btnCompleteOrder.Location = new System.Drawing.Point(208, 26);
            this.btnCompleteOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnCompleteOrder.Name = "btnCompleteOrder";
            this.btnCompleteOrder.Size = new System.Drawing.Size(200, 86);
            this.btnCompleteOrder.TabIndex = 2;
            this.btnCompleteOrder.Text = "Complete selected order";
            this.btnCompleteOrder.UseVisualStyleBackColor = true;
            this.btnCompleteOrder.Click += new System.EventHandler(this.btnCompleteOrder_Click);
            // 
            // dgvOrders
            // 
            this.dgvOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderId,
            this.orderCustomerId,
            this.orderCustomerInfo,
            this.orderProducts,
            this.orderPrice});
            this.dgvOrders.Location = new System.Drawing.Point(9, 124);
            this.dgvOrders.Margin = new System.Windows.Forms.Padding(4);
            this.dgvOrders.Name = "dgvOrders";
            this.dgvOrders.RowHeadersWidth = 51;
            this.dgvOrders.Size = new System.Drawing.Size(1444, 590);
            this.dgvOrders.TabIndex = 1;
            // 
            // orderId
            // 
            this.orderId.HeaderText = "Id:";
            this.orderId.MinimumWidth = 6;
            this.orderId.Name = "orderId";
            this.orderId.ReadOnly = true;
            this.orderId.Width = 60;
            // 
            // orderCustomerId
            // 
            this.orderCustomerId.HeaderText = "Customer id";
            this.orderCustomerId.MinimumWidth = 6;
            this.orderCustomerId.Name = "orderCustomerId";
            this.orderCustomerId.ReadOnly = true;
            this.orderCustomerId.Width = 60;
            // 
            // orderCustomerInfo
            // 
            this.orderCustomerInfo.HeaderText = "Customer information:";
            this.orderCustomerInfo.MinimumWidth = 6;
            this.orderCustomerInfo.Name = "orderCustomerInfo";
            this.orderCustomerInfo.ReadOnly = true;
            this.orderCustomerInfo.Width = 400;
            // 
            // orderProducts
            // 
            this.orderProducts.HeaderText = "Products:";
            this.orderProducts.MinimumWidth = 6;
            this.orderProducts.Name = "orderProducts";
            this.orderProducts.ReadOnly = true;
            this.orderProducts.Width = 430;
            // 
            // orderPrice
            // 
            this.orderPrice.HeaderText = "Total price:";
            this.orderPrice.MinimumWidth = 6;
            this.orderPrice.Name = "orderPrice";
            this.orderPrice.ReadOnly = true;
            this.orderPrice.Width = 90;
            // 
            // tpgPlaceOrder
            // 
            this.tpgPlaceOrder.Controls.Add(this.tbTotalPrice);
            this.tpgPlaceOrder.Controls.Add(this.lblTotalPrice);
            this.tpgPlaceOrder.Controls.Add(this.dgvProducts);
            this.tpgPlaceOrder.Controls.Add(this.gbPizzaCrust);
            this.tpgPlaceOrder.Controls.Add(this.lblCustomer);
            this.tpgPlaceOrder.Controls.Add(this.cbbCustomer);
            this.tpgPlaceOrder.Controls.Add(this.btnAddPizza);
            this.tpgPlaceOrder.Controls.Add(this.btnRemoveProduct);
            this.tpgPlaceOrder.Controls.Add(this.btnPlaceOrder);
            this.tpgPlaceOrder.Controls.Add(this.gbDrinks);
            this.tpgPlaceOrder.Controls.Add(this.gbPizzaTopping);
            this.tpgPlaceOrder.Location = new System.Drawing.Point(4, 25);
            this.tpgPlaceOrder.Margin = new System.Windows.Forms.Padding(4);
            this.tpgPlaceOrder.Name = "tpgPlaceOrder";
            this.tpgPlaceOrder.Padding = new System.Windows.Forms.Padding(4);
            this.tpgPlaceOrder.Size = new System.Drawing.Size(1491, 751);
            this.tpgPlaceOrder.TabIndex = 1;
            this.tpgPlaceOrder.Text = "Place order";
            this.tpgPlaceOrder.UseVisualStyleBackColor = true;
            // 
            // tbTotalPrice
            // 
            this.tbTotalPrice.Location = new System.Drawing.Point(389, 720);
            this.tbTotalPrice.Margin = new System.Windows.Forms.Padding(4);
            this.tbTotalPrice.Name = "tbTotalPrice";
            this.tbTotalPrice.ReadOnly = true;
            this.tbTotalPrice.Size = new System.Drawing.Size(99, 22);
            this.tbTotalPrice.TabIndex = 15;
            this.tbTotalPrice.TextChanged += new System.EventHandler(this.tbTotalPrice_TextChanged);
            // 
            // lblTotalPrice
            // 
            this.lblTotalPrice.AutoSize = true;
            this.lblTotalPrice.Location = new System.Drawing.Point(301, 725);
            this.lblTotalPrice.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalPrice.Name = "lblTotalPrice";
            this.lblTotalPrice.Size = new System.Drawing.Size(79, 17);
            this.lblTotalPrice.TabIndex = 14;
            this.lblTotalPrice.Text = "Total price:";
            // 
            // dgvProducts
            // 
            this.dgvProducts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducts.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.productId,
            this.productName,
            this.productPrice});
            this.dgvProducts.Location = new System.Drawing.Point(9, 241);
            this.dgvProducts.Margin = new System.Windows.Forms.Padding(4);
            this.dgvProducts.Name = "dgvProducts";
            this.dgvProducts.RowHeadersWidth = 51;
            this.dgvProducts.Size = new System.Drawing.Size(480, 480);
            this.dgvProducts.TabIndex = 13;
            // 
            // productId
            // 
            this.productId.HeaderText = "Id:";
            this.productId.MinimumWidth = 6;
            this.productId.Name = "productId";
            this.productId.ReadOnly = true;
            this.productId.Width = 125;
            // 
            // productName
            // 
            this.productName.HeaderText = "Name:";
            this.productName.MinimumWidth = 6;
            this.productName.Name = "productName";
            this.productName.ReadOnly = true;
            this.productName.Width = 170;
            // 
            // productPrice
            // 
            this.productPrice.HeaderText = "Price:";
            this.productPrice.MinimumWidth = 6;
            this.productPrice.Name = "productPrice";
            this.productPrice.ReadOnly = true;
            this.productPrice.Width = 50;
            // 
            // gbPizzaCrust
            // 
            this.gbPizzaCrust.Controls.Add(this.rbFilled);
            this.gbPizzaCrust.Controls.Add(this.rbThick);
            this.gbPizzaCrust.Controls.Add(this.rbThin);
            this.gbPizzaCrust.Location = new System.Drawing.Point(167, 52);
            this.gbPizzaCrust.Margin = new System.Windows.Forms.Padding(4);
            this.gbPizzaCrust.Name = "gbPizzaCrust";
            this.gbPizzaCrust.Padding = new System.Windows.Forms.Padding(4);
            this.gbPizzaCrust.Size = new System.Drawing.Size(193, 116);
            this.gbPizzaCrust.TabIndex = 12;
            this.gbPizzaCrust.TabStop = false;
            this.gbPizzaCrust.Text = "Pizza crust";
            // 
            // rbFilled
            // 
            this.rbFilled.AutoSize = true;
            this.rbFilled.Location = new System.Drawing.Point(9, 84);
            this.rbFilled.Margin = new System.Windows.Forms.Padding(4);
            this.rbFilled.Name = "rbFilled";
            this.rbFilled.Size = new System.Drawing.Size(62, 21);
            this.rbFilled.TabIndex = 2;
            this.rbFilled.TabStop = true;
            this.rbFilled.Text = "Filled";
            this.rbFilled.UseVisualStyleBackColor = true;
            // 
            // rbThick
            // 
            this.rbThick.AutoSize = true;
            this.rbThick.Location = new System.Drawing.Point(9, 54);
            this.rbThick.Margin = new System.Windows.Forms.Padding(4);
            this.rbThick.Name = "rbThick";
            this.rbThick.Size = new System.Drawing.Size(63, 21);
            this.rbThick.TabIndex = 1;
            this.rbThick.TabStop = true;
            this.rbThick.Text = "Thick";
            this.rbThick.UseVisualStyleBackColor = true;
            // 
            // rbThin
            // 
            this.rbThin.AutoSize = true;
            this.rbThin.Location = new System.Drawing.Point(9, 25);
            this.rbThin.Margin = new System.Windows.Forms.Padding(4);
            this.rbThin.Name = "rbThin";
            this.rbThin.Size = new System.Drawing.Size(57, 21);
            this.rbThin.TabIndex = 0;
            this.rbThin.TabStop = true;
            this.rbThin.Text = "Thin";
            this.rbThin.UseVisualStyleBackColor = true;
            // 
            // lblCustomer
            // 
            this.lblCustomer.AutoSize = true;
            this.lblCustomer.Location = new System.Drawing.Point(13, 22);
            this.lblCustomer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblCustomer.Name = "lblCustomer";
            this.lblCustomer.Size = new System.Drawing.Size(72, 17);
            this.lblCustomer.TabIndex = 11;
            this.lblCustomer.Text = "Customer:";
            // 
            // cbbCustomer
            // 
            this.cbbCustomer.FormattingEnabled = true;
            this.cbbCustomer.Location = new System.Drawing.Point(93, 18);
            this.cbbCustomer.Margin = new System.Windows.Forms.Padding(4);
            this.cbbCustomer.Name = "cbbCustomer";
            this.cbbCustomer.Size = new System.Drawing.Size(607, 24);
            this.cbbCustomer.TabIndex = 10;
            this.cbbCustomer.SelectedIndexChanged += new System.EventHandler(this.cbbCustomer_SelectedIndexChanged);
            // 
            // btnAddPizza
            // 
            this.btnAddPizza.Location = new System.Drawing.Point(167, 175);
            this.btnAddPizza.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddPizza.Name = "btnAddPizza";
            this.btnAddPizza.Size = new System.Drawing.Size(193, 58);
            this.btnAddPizza.TabIndex = 2;
            this.btnAddPizza.Text = "Add pizza";
            this.btnAddPizza.UseVisualStyleBackColor = true;
            this.btnAddPizza.Click += new System.EventHandler(this.btnAddPizza_Click);
            // 
            // btnRemoveProduct
            // 
            this.btnRemoveProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.btnRemoveProduct.Location = new System.Drawing.Point(497, 241);
            this.btnRemoveProduct.Margin = new System.Windows.Forms.Padding(4);
            this.btnRemoveProduct.Name = "btnRemoveProduct";
            this.btnRemoveProduct.Size = new System.Drawing.Size(204, 250);
            this.btnRemoveProduct.TabIndex = 9;
            this.btnRemoveProduct.Text = "Remove selected product";
            this.btnRemoveProduct.UseVisualStyleBackColor = true;
            this.btnRemoveProduct.Click += new System.EventHandler(this.btnRemoveProduct_Click);
            // 
            // btnPlaceOrder
            // 
            this.btnPlaceOrder.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.btnPlaceOrder.Location = new System.Drawing.Point(497, 497);
            this.btnPlaceOrder.Margin = new System.Windows.Forms.Padding(4);
            this.btnPlaceOrder.Name = "btnPlaceOrder";
            this.btnPlaceOrder.Size = new System.Drawing.Size(204, 244);
            this.btnPlaceOrder.TabIndex = 8;
            this.btnPlaceOrder.Text = "Place order";
            this.btnPlaceOrder.UseVisualStyleBackColor = true;
            this.btnPlaceOrder.Click += new System.EventHandler(this.btnPlaceOrder_Click);
            // 
            // gbDrinks
            // 
            this.gbDrinks.Controls.Add(this.rbEnergyDrink);
            this.gbDrinks.Controls.Add(this.rbIceTea);
            this.gbDrinks.Controls.Add(this.rbCola);
            this.gbDrinks.Controls.Add(this.rbSparklingWater);
            this.gbDrinks.Controls.Add(this.rbFlatWater);
            this.gbDrinks.Controls.Add(this.btnAddDrink);
            this.gbDrinks.Location = new System.Drawing.Point(368, 52);
            this.gbDrinks.Margin = new System.Windows.Forms.Padding(4);
            this.gbDrinks.Name = "gbDrinks";
            this.gbDrinks.Padding = new System.Windows.Forms.Padding(4);
            this.gbDrinks.Size = new System.Drawing.Size(333, 181);
            this.gbDrinks.TabIndex = 6;
            this.gbDrinks.TabStop = false;
            this.gbDrinks.Text = "Drinks";
            // 
            // rbEnergyDrink
            // 
            this.rbEnergyDrink.AutoSize = true;
            this.rbEnergyDrink.Location = new System.Drawing.Point(9, 142);
            this.rbEnergyDrink.Margin = new System.Windows.Forms.Padding(4);
            this.rbEnergyDrink.Name = "rbEnergyDrink";
            this.rbEnergyDrink.Size = new System.Drawing.Size(109, 21);
            this.rbEnergyDrink.TabIndex = 6;
            this.rbEnergyDrink.TabStop = true;
            this.rbEnergyDrink.Text = "Energy drink";
            this.rbEnergyDrink.UseVisualStyleBackColor = true;
            // 
            // rbIceTea
            // 
            this.rbIceTea.AutoSize = true;
            this.rbIceTea.Location = new System.Drawing.Point(9, 112);
            this.rbIceTea.Margin = new System.Windows.Forms.Padding(4);
            this.rbIceTea.Name = "rbIceTea";
            this.rbIceTea.Size = new System.Drawing.Size(71, 21);
            this.rbIceTea.TabIndex = 5;
            this.rbIceTea.TabStop = true;
            this.rbIceTea.Text = "Ice tea";
            this.rbIceTea.UseVisualStyleBackColor = true;
            // 
            // rbCola
            // 
            this.rbCola.AutoSize = true;
            this.rbCola.Location = new System.Drawing.Point(9, 84);
            this.rbCola.Margin = new System.Windows.Forms.Padding(4);
            this.rbCola.Name = "rbCola";
            this.rbCola.Size = new System.Drawing.Size(57, 21);
            this.rbCola.TabIndex = 4;
            this.rbCola.TabStop = true;
            this.rbCola.Text = "Cola";
            this.rbCola.UseVisualStyleBackColor = true;
            // 
            // rbSparklingWater
            // 
            this.rbSparklingWater.AutoSize = true;
            this.rbSparklingWater.Location = new System.Drawing.Point(9, 54);
            this.rbSparklingWater.Margin = new System.Windows.Forms.Padding(4);
            this.rbSparklingWater.Name = "rbSparklingWater";
            this.rbSparklingWater.Size = new System.Drawing.Size(126, 21);
            this.rbSparklingWater.TabIndex = 3;
            this.rbSparklingWater.TabStop = true;
            this.rbSparklingWater.Text = "Sparkling water";
            this.rbSparklingWater.UseVisualStyleBackColor = true;
            // 
            // rbFlatWater
            // 
            this.rbFlatWater.AutoSize = true;
            this.rbFlatWater.Location = new System.Drawing.Point(9, 25);
            this.rbFlatWater.Margin = new System.Windows.Forms.Padding(4);
            this.rbFlatWater.Name = "rbFlatWater";
            this.rbFlatWater.Size = new System.Drawing.Size(90, 21);
            this.rbFlatWater.TabIndex = 2;
            this.rbFlatWater.TabStop = true;
            this.rbFlatWater.Text = "Flat water";
            this.rbFlatWater.UseVisualStyleBackColor = true;
            // 
            // btnAddDrink
            // 
            this.btnAddDrink.Location = new System.Drawing.Point(129, 133);
            this.btnAddDrink.Margin = new System.Windows.Forms.Padding(4);
            this.btnAddDrink.Name = "btnAddDrink";
            this.btnAddDrink.Size = new System.Drawing.Size(196, 38);
            this.btnAddDrink.TabIndex = 1;
            this.btnAddDrink.Text = "Add drink";
            this.btnAddDrink.UseVisualStyleBackColor = true;
            this.btnAddDrink.Click += new System.EventHandler(this.btnAddDrink_Click);
            // 
            // gbPizzaTopping
            // 
            this.gbPizzaTopping.Controls.Add(this.rbBbqChicken);
            this.gbPizzaTopping.Controls.Add(this.rbPepperoni);
            this.gbPizzaTopping.Controls.Add(this.rbQuattroStagioni);
            this.gbPizzaTopping.Controls.Add(this.rbQuattroFormaggi);
            this.gbPizzaTopping.Controls.Add(this.rbMargherita);
            this.gbPizzaTopping.Location = new System.Drawing.Point(8, 52);
            this.gbPizzaTopping.Margin = new System.Windows.Forms.Padding(4);
            this.gbPizzaTopping.Name = "gbPizzaTopping";
            this.gbPizzaTopping.Padding = new System.Windows.Forms.Padding(4);
            this.gbPizzaTopping.Size = new System.Drawing.Size(151, 181);
            this.gbPizzaTopping.TabIndex = 5;
            this.gbPizzaTopping.TabStop = false;
            this.gbPizzaTopping.Text = "Pizza topping";
            // 
            // rbBbqChicken
            // 
            this.rbBbqChicken.AutoSize = true;
            this.rbBbqChicken.Location = new System.Drawing.Point(9, 143);
            this.rbBbqChicken.Margin = new System.Windows.Forms.Padding(4);
            this.rbBbqChicken.Name = "rbBbqChicken";
            this.rbBbqChicken.Size = new System.Drawing.Size(112, 21);
            this.rbBbqChicken.TabIndex = 7;
            this.rbBbqChicken.TabStop = true;
            this.rbBbqChicken.Text = "BBQ Chicken";
            this.rbBbqChicken.UseVisualStyleBackColor = true;
            // 
            // rbPepperoni
            // 
            this.rbPepperoni.AutoSize = true;
            this.rbPepperoni.Location = new System.Drawing.Point(9, 113);
            this.rbPepperoni.Margin = new System.Windows.Forms.Padding(4);
            this.rbPepperoni.Name = "rbPepperoni";
            this.rbPepperoni.Size = new System.Drawing.Size(94, 21);
            this.rbPepperoni.TabIndex = 6;
            this.rbPepperoni.TabStop = true;
            this.rbPepperoni.Text = "Pepperoni";
            this.rbPepperoni.UseVisualStyleBackColor = true;
            // 
            // rbQuattroStagioni
            // 
            this.rbQuattroStagioni.AutoSize = true;
            this.rbQuattroStagioni.Location = new System.Drawing.Point(9, 84);
            this.rbQuattroStagioni.Margin = new System.Windows.Forms.Padding(4);
            this.rbQuattroStagioni.Name = "rbQuattroStagioni";
            this.rbQuattroStagioni.Size = new System.Drawing.Size(132, 21);
            this.rbQuattroStagioni.TabIndex = 5;
            this.rbQuattroStagioni.TabStop = true;
            this.rbQuattroStagioni.Text = "Quattro Stagioni";
            this.rbQuattroStagioni.UseVisualStyleBackColor = true;
            // 
            // rbQuattroFormaggi
            // 
            this.rbQuattroFormaggi.AutoSize = true;
            this.rbQuattroFormaggi.Location = new System.Drawing.Point(9, 54);
            this.rbQuattroFormaggi.Margin = new System.Windows.Forms.Padding(4);
            this.rbQuattroFormaggi.Name = "rbQuattroFormaggi";
            this.rbQuattroFormaggi.Size = new System.Drawing.Size(140, 21);
            this.rbQuattroFormaggi.TabIndex = 4;
            this.rbQuattroFormaggi.TabStop = true;
            this.rbQuattroFormaggi.Text = "Quattro Formaggi";
            this.rbQuattroFormaggi.UseVisualStyleBackColor = true;
            // 
            // rbMargherita
            // 
            this.rbMargherita.AutoSize = true;
            this.rbMargherita.Location = new System.Drawing.Point(9, 25);
            this.rbMargherita.Margin = new System.Windows.Forms.Padding(4);
            this.rbMargherita.Name = "rbMargherita";
            this.rbMargherita.Size = new System.Drawing.Size(97, 21);
            this.rbMargherita.TabIndex = 3;
            this.rbMargherita.TabStop = true;
            this.rbMargherita.Text = "Margherita";
            this.rbMargherita.UseVisualStyleBackColor = true;
            // 
            // frmOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1525, 802);
            this.Controls.Add(this.tctrlCashRegister);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmOverview";
            this.Text = "Cash register";
            this.tctrlCashRegister.ResumeLayout(false);
            this.tpgCustomers.ResumeLayout(false);
            this.gbCustomer.ResumeLayout(false);
            this.gbCustomer.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCustomers)).EndInit();
            this.tpgOverview.ResumeLayout(false);
            this.gbOrders.ResumeLayout(false);
            this.gbOrders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvOrders)).EndInit();
            this.tpgPlaceOrder.ResumeLayout(false);
            this.tpgPlaceOrder.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducts)).EndInit();
            this.gbPizzaCrust.ResumeLayout(false);
            this.gbPizzaCrust.PerformLayout();
            this.gbDrinks.ResumeLayout(false);
            this.gbDrinks.PerformLayout();
            this.gbPizzaTopping.ResumeLayout(false);
            this.gbPizzaTopping.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tctrlCashRegister;
        private System.Windows.Forms.TabPage tpgOverview;
        private System.Windows.Forms.GroupBox gbOrders;
        private System.Windows.Forms.Button btnCompleteOrder;
        private System.Windows.Forms.DataGridView dgvOrders;
        private System.Windows.Forms.TabPage tpgPlaceOrder;
        private System.Windows.Forms.Button btnRemoveProduct;
        private System.Windows.Forms.Button btnPlaceOrder;
        private System.Windows.Forms.GroupBox gbDrinks;
        private System.Windows.Forms.Button btnAddDrink;
        private System.Windows.Forms.GroupBox gbPizzaTopping;
        private System.Windows.Forms.Button btnAddPizza;
        private System.Windows.Forms.TabPage tpgCustomers;
        private System.Windows.Forms.GroupBox gbCustomer;
        private System.Windows.Forms.DataGridView dgvCustomers;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerFirstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerLastName;
        private System.Windows.Forms.DataGridViewTextBoxColumn customerEmailAddress;
        private System.Windows.Forms.Button btnEditCustomer;
        private System.Windows.Forms.Button btnAddCustomer;
        private System.Windows.Forms.Label lblCustomerEmailAddress;
        private System.Windows.Forms.Label lblCustomerLastName;
        private System.Windows.Forms.Label lblCustomerFirstName;
        private System.Windows.Forms.TextBox tbFirstName;
        private System.Windows.Forms.TextBox tbEmailAddress;
        private System.Windows.Forms.TextBox tbLastName;
        private System.Windows.Forms.Label lblCustomer;
        private System.Windows.Forms.ComboBox cbbCustomer;
        private System.Windows.Forms.RadioButton rbBbqChicken;
        private System.Windows.Forms.RadioButton rbPepperoni;
        private System.Windows.Forms.RadioButton rbQuattroStagioni;
        private System.Windows.Forms.RadioButton rbQuattroFormaggi;
        private System.Windows.Forms.RadioButton rbMargherita;
        private System.Windows.Forms.GroupBox gbPizzaCrust;
        private System.Windows.Forms.RadioButton rbThin;
        private System.Windows.Forms.RadioButton rbFilled;
        private System.Windows.Forms.RadioButton rbThick;
        private System.Windows.Forms.RadioButton rbEnergyDrink;
        private System.Windows.Forms.RadioButton rbIceTea;
        private System.Windows.Forms.RadioButton rbCola;
        private System.Windows.Forms.RadioButton rbSparklingWater;
        private System.Windows.Forms.RadioButton rbFlatWater;
        private System.Windows.Forms.TextBox tbTotalPrice;
        private System.Windows.Forms.Label lblTotalPrice;
        private System.Windows.Forms.DataGridView dgvProducts;
        private System.Windows.Forms.Button btnSortByPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderId;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderCustomerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderCustomerInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn productId;
        private System.Windows.Forms.DataGridViewTextBoxColumn productName;
        private System.Windows.Forms.DataGridViewTextBoxColumn productPrice;
        private System.Windows.Forms.CheckBox cbShowCompletedOrders;
        private System.Windows.Forms.Button btnCancelOrder;
        private System.Windows.Forms.Button btnShowAllOrders;
    }
}

