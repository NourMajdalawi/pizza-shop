﻿namespace Week6assesmentassignment
{
    partial class frmAllOrdersAndRevenue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dtpFilterDate = new System.Windows.Forms.DateTimePicker();
            this.cbFilterByDate = new System.Windows.Forms.CheckBox();
            this.lblTotalRevenue = new System.Windows.Forms.Label();
            this.tbTotalRevenue = new System.Windows.Forms.TextBox();
            this.dgvAllOrders = new System.Windows.Forms.DataGridView();
            this.orderId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderCustomerId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderCustomerInfo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderProducts = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.orderPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSortOrdersByPrice = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllOrders)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpFilterDate
            // 
            this.dtpFilterDate.Location = new System.Drawing.Point(16, 15);
            this.dtpFilterDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dtpFilterDate.Name = "dtpFilterDate";
            this.dtpFilterDate.Size = new System.Drawing.Size(241, 22);
            this.dtpFilterDate.TabIndex = 1;
            this.dtpFilterDate.ValueChanged += new System.EventHandler(this.dtpFilterDate_ValueChanged);
            // 
            // cbFilterByDate
            // 
            this.cbFilterByDate.AutoSize = true;
            this.cbFilterByDate.Location = new System.Drawing.Point(267, 18);
            this.cbFilterByDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.cbFilterByDate.Name = "cbFilterByDate";
            this.cbFilterByDate.Size = new System.Drawing.Size(112, 21);
            this.cbFilterByDate.TabIndex = 2;
            this.cbFilterByDate.Text = "Filter by date";
            this.cbFilterByDate.UseVisualStyleBackColor = true;
            this.cbFilterByDate.CheckedChanged += new System.EventHandler(this.cbFilterByDate_CheckedChanged);
            // 
            // lblTotalRevenue
            // 
            this.lblTotalRevenue.AutoSize = true;
            this.lblTotalRevenue.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.lblTotalRevenue.Location = new System.Drawing.Point(1053, 7);
            this.lblTotalRevenue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblTotalRevenue.Name = "lblTotalRevenue";
            this.lblTotalRevenue.Size = new System.Drawing.Size(212, 35);
            this.lblTotalRevenue.TabIndex = 3;
            this.lblTotalRevenue.Text = "Total revenue:";
            // 
            // tbTotalRevenue
            // 
            this.tbTotalRevenue.Font = new System.Drawing.Font("Microsoft Sans Serif", 18.25F);
            this.tbTotalRevenue.Location = new System.Drawing.Point(1293, 4);
            this.tbTotalRevenue.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tbTotalRevenue.Name = "tbTotalRevenue";
            this.tbTotalRevenue.Size = new System.Drawing.Size(165, 42);
            this.tbTotalRevenue.TabIndex = 4;
            this.tbTotalRevenue.TextChanged += new System.EventHandler(this.tbTotalRevenue_TextChanged);
            // 
            // dgvAllOrders
            // 
            this.dgvAllOrders.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAllOrders.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.orderId,
            this.orderCustomerId,
            this.orderCustomerInfo,
            this.orderProducts,
            this.orderPrice});
            this.dgvAllOrders.Location = new System.Drawing.Point(16, 54);
            this.dgvAllOrders.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dgvAllOrders.Name = "dgvAllOrders";
            this.dgvAllOrders.RowHeadersWidth = 51;
            this.dgvAllOrders.Size = new System.Drawing.Size(1444, 713);
            this.dgvAllOrders.TabIndex = 5;
            // 
            // orderId
            // 
            this.orderId.HeaderText = "Id:";
            this.orderId.MinimumWidth = 6;
            this.orderId.Name = "orderId";
            this.orderId.ReadOnly = true;
            this.orderId.Width = 60;
            // 
            // orderCustomerId
            // 
            this.orderCustomerId.HeaderText = "Customer id";
            this.orderCustomerId.MinimumWidth = 6;
            this.orderCustomerId.Name = "orderCustomerId";
            this.orderCustomerId.ReadOnly = true;
            this.orderCustomerId.Width = 60;
            // 
            // orderCustomerInfo
            // 
            this.orderCustomerInfo.HeaderText = "Customer information:";
            this.orderCustomerInfo.MinimumWidth = 6;
            this.orderCustomerInfo.Name = "orderCustomerInfo";
            this.orderCustomerInfo.ReadOnly = true;
            this.orderCustomerInfo.Width = 400;
            // 
            // orderProducts
            // 
            this.orderProducts.HeaderText = "Products:";
            this.orderProducts.MinimumWidth = 6;
            this.orderProducts.Name = "orderProducts";
            this.orderProducts.ReadOnly = true;
            this.orderProducts.Width = 430;
            // 
            // orderPrice
            // 
            this.orderPrice.HeaderText = "Total price:";
            this.orderPrice.MinimumWidth = 6;
            this.orderPrice.Name = "orderPrice";
            this.orderPrice.ReadOnly = true;
            this.orderPrice.Width = 90;
            // 
            // btnSortOrdersByPrice
            // 
            this.btnSortOrdersByPrice.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.btnSortOrdersByPrice.Location = new System.Drawing.Point(631, 11);
            this.btnSortOrdersByPrice.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnSortOrdersByPrice.Name = "btnSortOrdersByPrice";
            this.btnSortOrdersByPrice.Size = new System.Drawing.Size(415, 32);
            this.btnSortOrdersByPrice.TabIndex = 6;
            this.btnSortOrdersByPrice.Text = "Sort orders on price";
            this.btnSortOrdersByPrice.UseVisualStyleBackColor = true;
            this.btnSortOrdersByPrice.Click += new System.EventHandler(this.btnSortOrdersByPrice_Click);
            // 
            // frmAllOrdersAndRevenue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1477, 782);
            this.Controls.Add(this.btnSortOrdersByPrice);
            this.Controls.Add(this.dgvAllOrders);
            this.Controls.Add(this.tbTotalRevenue);
            this.Controls.Add(this.lblTotalRevenue);
            this.Controls.Add(this.cbFilterByDate);
            this.Controls.Add(this.dtpFilterDate);
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "frmAllOrdersAndRevenue";
            this.Text = "frmAllOrdersAndRevenue";
            ((System.ComponentModel.ISupportInitialize)(this.dgvAllOrders)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DateTimePicker dtpFilterDate;
        private System.Windows.Forms.CheckBox cbFilterByDate;
        private System.Windows.Forms.Label lblTotalRevenue;
        private System.Windows.Forms.TextBox tbTotalRevenue;
        private System.Windows.Forms.DataGridView dgvAllOrders;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderId;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderCustomerId;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderCustomerInfo;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderProducts;
        private System.Windows.Forms.DataGridViewTextBoxColumn orderPrice;
        private System.Windows.Forms.Button btnSortOrdersByPrice;
    }
}